package com.ju.cardslider.offer_model;

public class Offers {

    private  String offerName, OfferCompany,offerDescription,OfferTime,OfferLocation,offerLat,offerLng;

    public Offers(){

    }

    public String getOfferName() {
        return offerName;
    }

    public void setOfferName(String offerName) {
        this.offerName = offerName;
    }

    public String getOfferCompany() {
        return OfferCompany;
    }

    public void setOfferCompany(String offerCompany) {
        OfferCompany = offerCompany;
    }

    public String getOfferDescription() {
        return offerDescription;
    }

    public void setOfferDescription(String offerDescription) {
        this.offerDescription = offerDescription;
    }

    public String getOfferTime() {
        return OfferTime;
    }

    public void setOfferTime(String offerTime) {
        OfferTime = offerTime;
    }

    public String getOfferLocation() {
        return OfferLocation;
    }

    public void setOfferLocation(String offerLocation) {
        OfferLocation = offerLocation;
    }

    public String getOfferLat() {
        return offerLat;
    }

    public void setOfferLat(String offerLat) {
        this.offerLat = offerLat;
    }

    public String getOfferLng() {
        return offerLng;
    }

    public void setOfferLng(String offerLng) {
        this.offerLng = offerLng;
    }
}
