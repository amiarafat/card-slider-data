package com.ju.cardslider.helper;


import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.DatabaseReference;
import com.ju.cardslider.offer_model.Offers;

import java.util.ArrayList;

public class FirebaseHelper {

    DatabaseReference db;
    Boolean saved;
    ArrayList<Offers> offers=new ArrayList<>();

    public FirebaseHelper(DatabaseReference db) {
        this.db = db;
    }

    //WRITE IF NOT NULL
    public Boolean save(Offers offers)
    {
        if(offers==null)
        {
            saved=false;
        }else
        {
            try
            {
                db.child("offers").push().setValue(offers);
                saved=true;

            }catch (DatabaseException e)
            {
                e.printStackTrace();
                saved=false;
            }
        }

        return saved;
    }


    //IMPLEMENT FETCH DATA AND FILL ARRAYLIST
    private void fetchData(DataSnapshot dataSnapshot)
    {
        offers.clear();

        for (DataSnapshot ds : dataSnapshot.getChildren())
        {
            Offers offer=ds.getValue(Offers.class);
            offers.add(offer);
        }
    }

    //RETRIEVE
    public ArrayList<Offers> retrieve()
    {
        db.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

                Log.d("data1::",dataSnapshot.toString());

                fetchData(dataSnapshot);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                fetchData(dataSnapshot);

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return offers;
    }

}
