package com.ju.cardslider;

import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.ju.cardslider.helper.FirebaseHelper;
import com.ju.cardslider.offer_model.Offers;

public class NewOffer extends FragmentActivity implements OnMapReadyCallback {


    TextView tvGetLocation;
    int PLACE_PICKER_REQUEST = 1;
    private GoogleMap mMap;
    TableRow trMap;

    EditText etName, etOComp,etDesc,etTime;
    Button btnSubmit;

    String lat,lng;
    private String addressPD = "";

    DatabaseReference db;
    FirebaseHelper helper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_offer);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.Nmap);
        mapFragment.getMapAsync(this);

        //INITIALIZE FIREBASE DB
        db = FirebaseDatabase.getInstance().getReference();
        helper = new FirebaseHelper(db);


        tvGetLocation = findViewById(R.id.tvGetLocation);
        trMap = findViewById(R.id.trMap);
        etName = findViewById(R.id.etName);
        etOComp = findViewById(R.id.etComp);
        etDesc = findViewById(R.id.etODesc);
        etTime = findViewById(R.id.etTime);
        btnSubmit = findViewById(R.id.btnSubmit);

        tvGetLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    openGooglePlaceApiUiForPickUP();
                } catch (GooglePlayServicesNotAvailableException e) {
                    e.printStackTrace();
                } catch (GooglePlayServicesRepairableException e) {
                    e.printStackTrace();
                }
            }
        });


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String oName = etName.getText().toString();
                String oComp = etOComp.getText().toString();
                String oDesc = etDesc.getText().toString();
                String oTime = etTime.getText().toString();

                if(!TextUtils.isEmpty(oName) && !TextUtils.isEmpty(oComp) && !TextUtils.isEmpty(oDesc) && !TextUtils.isEmpty(oTime) && !TextUtils.isEmpty(addressPD) && !TextUtils.isEmpty(lat) && !TextUtils.isEmpty(lng) ){

                    Offers of = new Offers();

                    of.setOfferName(oName);
                    of.setOfferCompany(oComp);
                    of.setOfferCompany(oComp);
                    of.setOfferDescription(oDesc);
                    of.setOfferTime(oTime);
                    of.setOfferLocation(addressPD);
                    of.setOfferLat(lat);
                    of.setOfferLng(lng);

                    helper.save(of);


                    Intent in =new Intent( NewOffer.this,MainActivity.class);
                    startActivity(in);
                    finish();

                }else {

                    Toast.makeText(getApplicationContext(),"Please enter all the info along with location from map. Thank You.",Toast.LENGTH_LONG).show();
                }



            }
        });
    }

    private void openGooglePlaceApiUiForPickUP() throws GooglePlayServicesNotAvailableException, GooglePlayServicesRepairableException {

        PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
        startActivityForResult(builder.build(NewOffer.this), PLACE_PICKER_REQUEST);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        Log.d("reId::", requestCode + "");

        if (requestCode == PLACE_PICKER_REQUEST) {

            if (resultCode == RESULT_OK) {
                Place place = PlacePicker.getPlace(data, NewOffer.this);
                Log.d("pl::", place.toString());
                Log.d("pl_la::", place.getLatLng().toString());

                addressPD = place.getAddress().toString();
                Log.d("place:::", addressPD);

                if(place.getLatLng()!= null){

                    lat =String.valueOf(place.getLatLng().latitude);
                    lng =String.valueOf(place.getLatLng().longitude);

                    animatetoLocation(place.getLatLng());
                    trMap.setVisibility(View.VISIBLE);
                    tvGetLocation.setText(addressPD);
                    tvGetLocation.setTextSize(14);
                }

            }
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }

    private void animatetoLocation(LatLng ltln){

        CameraUpdate location = CameraUpdateFactory.newLatLngZoom(
                ltln, 15);
        mMap.animateCamera(location);
        mMap.addMarker(new MarkerOptions()
                .position(ltln));


    }
}
